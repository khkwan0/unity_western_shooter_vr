﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeController : MonoBehaviour {

    // Use this for initialization

    private float y;
	void Start () {
        y = 0f;
        //GetComponent<Rigidbody>().AddForce(0f, 100f, 100f);
        Invoke("Kick", 5f);
	}
	
	// Update is called once per frame
	void Update () {
        //transform.rotation = Quaternion.Euler(0f, y, 0f);
        y += 0.1f;
	}

    void Kick()
    {
        GetComponent<Rigidbody>().AddForce(0f, 0f, 100f);
    }
}
