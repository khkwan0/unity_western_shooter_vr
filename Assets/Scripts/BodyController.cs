﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyController : MonoBehaviour {

    private Collider bodyCollier;
    [SerializeField]
    private float headHeight;
    public GameObject playerHead;
    private Vector3 headPos = new Vector3();

    [SerializeField]
    private Vector3 playerHeadPops;

    private Vector3 bodySize = new Vector3();

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        playerHeadPops = playerHead.transform.position;
        //headHeight = playerHead.transform.position.y;
        //transform.position = playerHead.transform.position;
        //transform.position = transform.position - new Vector3(0f, playerHead.transform.position.y/2f,0f);
        //transform.localScale = new Vector3(transform.localScale.x, playerHead.transform.position.y * 2f, transform.localScale.z);
        headPos.Set(playerHead.transform.position.x, playerHead.transform.position.y / 2f, playerHead.transform.position.z);
        bodySize.Set(GetComponent<BoxCollider>().size.x, playerHead.transform.position.y / 1.6f, GetComponent<BoxCollider>().size.z);

        transform.position = headPos;
        GetComponent<BoxCollider>().size = bodySize;
	}
}
