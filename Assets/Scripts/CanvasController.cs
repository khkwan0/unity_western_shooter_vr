﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CanvasController : MonoBehaviour {

    private TextMeshProUGUI scoreDisplay;
    private TextMeshProUGUI lifeDisplay;
    public TextMeshProUGUI battery;
    public Slider slowSlider;
    public Slider laserSlider;
    private int score;
    public RectTransform scoreArea;
    public RectTransform lifeArea;
    private int currentHitPoints = 0;
    public GameObject sliderKnob;
    private bool isChangingHitPoints = false;
    public TextMeshProUGUI counter;
    public TextMeshProUGUI debug;

	// Use this for initialization
	void Awake () {
        score = 0;
        scoreDisplay = scoreArea.GetComponent<TextMeshProUGUI>();
        scoreDisplay.text = score.ToString();
        lifeDisplay = lifeArea.GetComponent<TextMeshProUGUI>();
	}
	
    public void AddScore(int amount)
    {
        score += amount;
        scoreDisplay.text = score.ToString();
    }

    public void AdjustHitPoints(int hitPoints)
    {
        StartCoroutine(_ChangeHitPoints(hitPoints));
    }

    IEnumerator _ChangeHitPoints(int hitPoints)
    {
        while (isChangingHitPoints)
        {
            yield return null;
        }
        isChangingHitPoints = true;
        StartCoroutine(ChangeHitPoints(hitPoints));
    }

    IEnumerator ChangeHitPoints(int hitPoints)
    {
        while (currentHitPoints != hitPoints)
        {
            if (currentHitPoints < hitPoints)
            {
                currentHitPoints++;
            }
            else
            {
                currentHitPoints--;
            }
            lifeDisplay.text = currentHitPoints.ToString();
            yield return null;
        }
        isChangingHitPoints = false;
    }

    public void SetTimeScaleSlider(int amt)
    {
        slowSlider.GetComponent<Slider>().value = amt;
    }

    public void SetTimeScaleSliderColor(Color _color)
    {
        sliderKnob.GetComponent<Image>().color = _color;
    }

    public void SetMaxLaserBattery(int amt)
    {
        laserSlider.GetComponent<Slider>().maxValue = amt;
    }

    public void SetLaserSlider(int amt)
    {
        battery.text = amt.ToString();
        laserSlider.GetComponent<Slider>().value = amt;
    }

    public void SetCounter(int val)
    {        
        counter.text = val.ToString();
    }

    public void ShowHud()
    {
        slowSlider.transform.gameObject.SetActive(true);
        laserSlider.transform.gameObject.SetActive(true);
        scoreArea.transform.gameObject.SetActive(true);
        lifeArea.transform.gameObject.SetActive(true);
    }

    public void SetShowLaserSlider(bool on)
    {                
        laserSlider.transform.gameObject.SetActive(on);      
    }

    public void HideHud()
    {
        slowSlider.transform.gameObject.SetActive(false);
        laserSlider.transform.gameObject.SetActive(false);
        scoreArea.transform.gameObject.SetActive(false);
        lifeArea.transform.gameObject.SetActive(false);
    }
    public void ShowCounter()
    {
        counter.transform.gameObject.SetActive(true);
    }

    public void HideCounter()
    {
        counter.transform.gameObject.SetActive(false);
    }

    public void ShowDebug()
    {
        debug.transform.gameObject.SetActive(true);
    }

    public void HideDebug()
    {
        debug.transform.gameObject.SetActive(false);
    }

    public void SetDebugText(string text)
    {
        debug.text = text;
    }
}
