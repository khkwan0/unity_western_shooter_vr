﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BellHolderController : MonoBehaviour {

    public GameObject bell;
	// Use this for initialization
	void Start () {
        GetComponent<TargetController>().onHit += DoHit;
        //Invoke("Hitit", 7f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void Hitit()
    {
        DoHit(Vector3.zero, Vector3.zero, null, 0, 0f);
    }

    public void DoHit(Vector3 where, Vector3 normal, Transform shooter, int damage, float forceAmt)
    {
        Destroy(bell.GetComponent<HingeJoint>());
        bell.GetComponent<Rigidbody>().AddForceAtPosition(new Vector3(0f, 0f, -500f), bell.transform.position + new Vector3(0f, 0.4f, 0f), ForceMode.Acceleration);
    }
}
