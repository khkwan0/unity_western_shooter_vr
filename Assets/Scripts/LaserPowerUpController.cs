﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserPowerUpController : MonoBehaviour
{
    public int rechargeAmt;
    public bool shouldRotate = true;
    public float spinSpeed = 1f;
    public bool falling = true;
    public float fallSpeed;
    public Vector3 rotationAxis;
    private Vector3 rotation = new Vector3();
    public float approachSpeed;

    // Use this for initialization
    void Start()
    {
        if (GetComponent<TargetController>() != null)
        {
            GetComponent<TargetController>().onHit += HandleHit;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (shouldRotate)
        {
            rotation.Set(rotationAxis.x * spinSpeed, rotationAxis.y * spinSpeed, rotationAxis.z * spinSpeed);
            transform.Rotate(rotation);
        }
        if (falling)
        {
            transform.position += Vector3.down * fallSpeed;
        }
    }

    public void HandleHit(Vector3 where, Vector3 normal, Transform shooterPosition, int damage, float force)
    {
        falling = false;
        shouldRotate = false;
        GetComponent<BoxCollider>().size = new Vector3(2f, 2f, 2f);
        GetComponent<BoxCollider>().center = new Vector3(0f, 0f, 0f);
        StartCoroutine(MoveToShooter(shooterPosition.transform.position));
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "head" || collision.collider.tag == "body")
        {
            //collision.collider.transform.gameObject.GetComponent<TargetController>().DoHit(Vector3.zero, Vector3.zero, transform, 0);
            GameObject.Find("SceneController").GetComponent<SceneController>().SetLaserSightsBattery(rechargeAmt);
            Destroy(transform.gameObject);
        }
    }


    IEnumerator MoveToShooter(Vector3 finalPos)
    {
        Vector3 currPos = transform.position;
        transform.LookAt(finalPos);
        while (Vector3.Distance(currPos, finalPos) > 0f)
        {
            GetComponent<Rigidbody>().velocity += transform.forward * approachSpeed;
            currPos = transform.position;
            yield return null;
        }
    }
}
