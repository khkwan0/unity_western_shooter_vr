﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DuelPositionController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "enemy")
        {
            if (other.transform.parent.gameObject.GetComponent<EnemyController>().Dueling)
            {
                other.transform.parent.transform.gameObject.GetComponent<Animator>().Play("Idle_ReadyToNeutral");
                other.transform.parent.transform.LookAt(other.transform.parent.GetComponent<EnemyController>().player.transform);
                GameObject.Find("SceneController").GetComponent<SceneController>().InDuelPosition = true;
            }

        }
    }
}
