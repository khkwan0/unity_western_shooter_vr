﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChickenController : MonoBehaviour {

    [SerializeField]
    private int state;
    private Animator anim;
    private AudioSource audioSource;
    public AudioClip cluckClip;
    public GameObject hitEffect;

	// Use this for initialization
	void Start () {
        state = 0;
        anim = GetComponent<Animator>();
        InvokeRepeating("ChangeState", 5f, 5f);
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = cluckClip;
        audioSource.loop = true;
        audioSource.Play();
        GetComponent<TargetController>().onHit += Hit;
	}

    public void Hit(Vector3 where, Vector3 normal, Transform shooter, int damage, float force)
    {
        audioSource.loop = false;
        GetComponent<BoxCollider>().enabled = false;
        GetComponent<Rigidbody>().useGravity = false;
        GameObject.Instantiate(hitEffect, transform.position, Quaternion.identity, transform);
        state = 4; // dead
        CancelInvoke("ChangeState");
        anim.Play("Dying");

        Destroy(transform.gameObject, 10f);
    }

    private void Update()
    {
        if (state == 2)
        {
            transform.position += transform.forward * 0.002f;
        }
    }

    private void ChangeState()
    {
        if (state == 1)
        {
            state = 3;
            anim.Play("StopPecking");
        }
        state = Random.Range(0, 3);
        switch (state) {
            case 0: anim.Play("Idle"); break;
            case 1: anim.Play("Peck"); break;
            case 2: anim.Play("Walk"); break;
            default: anim.Play("Idle"); break;
        }
    }
        
}
