﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shoot : MonoBehaviour
{

    private SteamVR_TrackedController controller;
    public Transform muzzlePosition;
    public GameObject shotEffect;
    public GameObject hitEffect;
    public AudioClip gunshotSound;
    public AudioClip emptySound;
    public AudioClip finishReloadSound;
    public AudioSource localAudioSource;
    public GameObject drum;
    public GameObject sceneController;
    public GameObject floatingScore;
    public GameObject player;
    public int shootDamage = 0;
    private int ammoCount;
    public int maxAmmo;
    public float reloadTime;
    [SerializeField]
    private bool canShoot;
    [SerializeField]
    private bool autoReload;

    public bool isHairTrigger = true;

    private int dynamiteHits;
    private Vector3 originalDrumPos;

    public int AmmoCount
    {
        set { ammoCount = value; }
        get { return ammoCount; }
    }

    public bool AutoReload
    {
        set { autoReload = value; }
        get { return autoReload; }
    }

    public bool CanShoot
    {
        set { canShoot = value; }
        get { return canShoot; }
    }

    // Use this for initialization
    void Start()
    {
        controller = GetComponent<SteamVR_TrackedController>();
        controller.TriggerClicked += Shoot;
        controller.Gripped += SlowTime;
        controller.Ungripped += ResumeTime;
        shotEffect.transform.position = muzzlePosition.transform.position;
        dynamiteHits = 0;
        originalDrumPos = drum.transform.localPosition;
        ammoCount = maxAmmo;
        //canShoot = true;
        //autoReload = true;
    }

    private void SlowTime(object sender, ClickedEventArgs e)
    {
        sceneController.GetComponent<SceneController>().GetSlow();
    }

    private void ResumeTime(object sender, ClickedEventArgs e)
    {
        sceneController.GetComponent<SceneController>().ResumeTime();
    }

    private void Shoot(object sender, ClickedEventArgs e)
    {
        if (canShoot)
        {
            if (isHairTrigger)
            {
                if (ammoCount > 0)
                {
                    RaycastHit hit;

                    localAudioSource.clip = gunshotSound;
                    localAudioSource.Play();
                    shotEffect.GetComponent<ParticleSystem>().Play();
                    drum.transform.Rotate(Vector3.right, 60.0f);
                    if (Physics.Raycast(muzzlePosition.position, muzzlePosition.forward, out hit, 500))
                    {
                        if (hit.collider != null)
                        {
                            TargetController targetController = null;
                            targetController = hit.collider.GetComponent<TargetController>();
                            if (targetController == null && hit.collider.transform.parent != null)
                            {
                                targetController = hit.collider.transform.parent.gameObject.GetComponent<TargetController>();
                            }
                            if (targetController != null)
                            {
                                targetController.DoHit(hit.point, hit.normal, muzzlePosition, shootDamage, 500f);
                            }
                            else
                            {
                                GameObject.Instantiate(hitEffect, hit.point, Quaternion.Euler(hit.normal));
                            }
                        }
                    }
                    if (!sceneController.GetComponent<SceneController>().EasyMode)
                    {
                        ammoCount--;
                    }
                    if (ammoCount == 0)
                    {
                        Vector3 newDrumPos = drum.transform.position;
                        newDrumPos.Set(drum.transform.localPosition.x, drum.transform.localPosition.y + 0.00256f, drum.transform.localPosition.z);
                        drum.transform.localPosition = newDrumPos;
                        if (autoReload)
                        {
                            StartCoroutine(Reload());
                        }
                    }
                }
                else
                {
                    sceneController.GetComponent<SceneController>().PlaySound(emptySound);
                }
            }
        }
    }

    IEnumerator Reload()
    {

        yield return new WaitForSeconds(reloadTime);
        localAudioSource.clip = finishReloadSound;
        localAudioSource.Play();
        yield return new WaitForSeconds(1f);
        ammoCount = maxAmmo;
        drum.transform.localPosition = originalDrumPos;
    }
}

