﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopAndAttackTriggerController : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "enemy")
        {
            other.transform.parent.gameObject.GetComponent<EnemyController>().AttackEnabled = true;
            other.transform.parent.gameObject.GetComponent<EnemyController>().StopMoving();
        }
    }
}
