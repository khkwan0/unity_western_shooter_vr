﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrelController : MonoBehaviour {

    public bool isPrimed;
    public float radius = 50f;
    public int explosiveDamage;

	// Use this for initialization
	void Start () {
        GetComponent<TargetController>().onHit += AddExplosiveForceOnDestroy;
        isPrimed = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void AddExplosiveForceOnDestroy(Vector3 where, Vector3 normal, Transform shooter, int damage, float forceAmt)
    {
        if (isPrimed)
        {
            GetComponent<TargetController>().hitPoints--;
            if (GetComponent<TargetController>().hitPoints <= 0)
            {
                Collider[] colliders = Physics.OverlapSphere(where, radius);
                foreach (Collider c in colliders)
                {
                    Debug.Log(c.gameObject.name);
                    if (c.GetComponent<Rigidbody>() == null) continue;
                    c.GetComponent<Rigidbody>().AddExplosionForce(50f, where, radius, 100f, ForceMode.Impulse);
                    if (c.tag == "enemy" )
                    {
                        c.GetComponent<EnemyController>().TakeExplosiveDamage(explosiveDamage);
                    }
                }
                GetComponent<MeshRenderer>().enabled = false;
                GetComponent<MeshCollider>().enabled = false;
                Destroy(transform.gameObject, 3f);
            }
        }
    }

    public void Explode()
    {
        isPrimed = true;
        AddExplosiveForceOnDestroy(transform.position, transform.up, null, 0, 0f);
    }
}
