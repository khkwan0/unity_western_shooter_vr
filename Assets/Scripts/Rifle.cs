﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rifle : MonoBehaviour {

    private SteamVR_TrackedController controller;
    public Camera eyeView;

    // Use this for initialization
    void Start () {
	    controller = GetComponent<SteamVR_TrackedController>();
        controller.TriggerClicked += Shoot;
        controller.Gripped += Aim;
        controller.Ungripped += UnAim;

        
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void Aim(object sender, ClickedEventArgs args) 
    {
        eyeView.GetComponent<Camera>().fieldOfView = 30f;
    }

    private void Shoot(object sender, ClickedEventArgs args)
    {
        Debug.Log("Shoot");
    }

    private void UnAim(object sender, ClickedEventArgs args)
    {
        eyeView.GetComponent<Camera>().fieldOfView = 60f;
    }
}
