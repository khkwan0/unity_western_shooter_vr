﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneController : MonoBehaviour {

    private int dynamiteHits;
    public GameObject leftLasterSight, rightLaserSight;
    public RectTransform canvas;
    private CanvasController canvasController;
    public GameObject floatingScore;
    public GameObject heart;
    public GameObject laserPowerUp;
    public GameObject heartSpawnLocation;
    public Vector3 heartSpawnVolumeRadii;
    [SerializeField]
    private int playerHitPoints;
    public int timeBank = 500;
    private int maxTimeBank;
    private bool timeBankRefilling = false;
    public int maxHP = 100;
    public GameObject _soundController;
    private SoundController soundController;
    public GameObject _musicController;
    private MusicController musicController;
    public GameObject enemyPrefab;    
    //public GameObject _groundEnemy;
    private GameObject groundEnemy;
    public Transform groundEnemySpawnPoint;
    //public GameObject _roofTopEnemy;
    private GameObject roofTopEnemy = null;
    private GameObject roofTopEnemy2 = null;
    public Transform roofTopSpawn;
    public Transform roofTopSpawn2;
    public int laserBatteryMax;
    public GameObject canPyramid;
    public GameObject easyModeTarget;
    [SerializeField]
    private bool easyMode;
    [SerializeField]
    private int laserBatteryAmt;
    [SerializeField]
    private bool isInGame;
    public Transform[] chickenSpawnPoints;
    public GameObject _chicken;
    private bool timeSlowed = false;
    public GameObject player;
    public GameObject leftPistol;
    public GameObject rightPistol;
    public GameObject leftPistolController;
    public GameObject rightPistolController;
    public AudioClip duelStartedSoundClip;
    private int enemyCount = 0;

    public bool debugMode = false;

    // for dueling
    public bool duelMode = false;
    private bool drawn;
    private bool duelStarted;
    private int holsterCount;
    private bool inDuelingPosition;
    private bool readyForDuel;
    private bool waitForHolster;

    public bool InDuelPosition
    {
        set { inDuelingPosition = value; }
        get { return inDuelingPosition; }
    }

    public GameObject GetPlayer()
    {
        return player;
    }

    public bool EasyMode
    {
        get { return easyMode; }
    }

    public bool IsInGame
    {
        get { return isInGame; }
        set { isInGame = value; }
    }

    public void IncrementEnemyCount()
    {
        enemyCount++;
    }

    public void DecrementEnemyCount()
    {
        enemyCount--;
    }

    private void Start()
    {        
        isInGame = false;
        musicController = _musicController.GetComponent<MusicController>();
        musicController.PlayIntro();
        soundController = _soundController.GetComponent<SoundController>();
        canvasController = canvas.GetComponent<CanvasController>();
        EnableLaserSights(true);
        inDuelingPosition = false;
        leftPistolController.GetComponent<shoot>().CanShoot = true;
        leftPistolController.GetComponent<shoot>().AutoReload = true;
        rightPistolController.GetComponent<shoot>().CanShoot = true;
        rightPistolController.GetComponent<shoot>().AutoReload = true;
        Invoke("EnableShowDownMode", 5f);
    }

    public void DebugOnScreen(string txt)
    {
        canvasController.SetDebugText(txt);
    }

   
    private void Restart()
    {
        easyModeTarget.gameObject.SetActive(true);
    }

    public void StartEasyMode()
    {
        if (!isInGame)
        {
            easyMode = true;
            _DoCountDown(3);
        }
    }

    public void StartNormalMode()
    {
        if (!isInGame)
        {
            EnableLaserSights(false);
            easyMode = false;
            _DoCountDown(3);
        }
    }

    public void _DoCountDown(int seconds)
    {
        canvasController.HideHud();
        canvasController.ShowCounter();
        StartCoroutine(DoCountDown(seconds));
    }

    IEnumerator DoCountDown(int startValue)
    {
        yield return new WaitForSeconds(1f);
        while (startValue >= 0)
        {
            canvasController.SetCounter(startValue);
            startValue--;
            yield return new WaitForSeconds(1f);
        }
        DoStart();
    }

    void DoStart () {
        isInGame = true;
        easyModeTarget.gameObject.SetActive(false);
        musicController.PlayNormal();
        maxTimeBank = timeBank;
        Time.timeScale = 1f;
        dynamiteHits = 0;
        InvokeRepeating("SpawnHeart", 5f, 5f);
        if (!easyMode)
        {
            InvokeRepeating("SpawnLaser", 5f, 20f);
        }
        canvasController.HideCounter();
        canvasController.ShowHud();
        if (easyMode)
        {
            canvasController.SetShowLaserSlider(false);
        }
        canvasController.AdjustHitPoints(playerHitPoints);
        canvasController.SetTimeScaleSlider(timeBank);
        canvasController.SetMaxLaserBattery(laserBatteryMax);
        canvasController.SetLaserSlider(laserBatteryAmt);
        soundController = _soundController.GetComponent<SoundController>();
        playerHitPoints = maxHP;
        laserBatteryAmt = 0;
        SpawnGroundEnemy();        
        if (!easyMode)
        {
            SpawnRoofTopEnemy1();
            SpawnRoofTopEnemy2();
        }
        for (int i = 0; i < chickenSpawnPoints.Length; i++)
        {
            GameObject.Instantiate(_chicken, chickenSpawnPoints[i].position, chickenSpawnPoints[i].rotation, GameObject.Find("Town").transform);
        }
	}

    private void SpawnGroundEnemy()
    {
        EnemyParams enemyParams = new EnemyParams();
        enemyParams.canThrow = true;
        enemyParams.canShoot = true;
        enemyParams.autoAttackShoot = true;
        enemyParams.autoAttackThrow = true;
        enemyParams.autoRespawn = true;
        enemyParams.bulletProof = false;
        enemyParams.explosiveProof = false;
        enemyParams.counterShoot = true;
        enemyParams.intialAttackEnabled = true;
        enemyParams.stationaryAfterStop = false;
        enemyParams.maxHitPoints = 100;
        enemyParams.explosiveProof = false;
        enemyParams.freezeZPos = true;
        SpawnEnemy(0f, enemyParams, groundEnemySpawnPoint);
    }

    private void SpawnRoofTopEnemy1()
    {
        EnemyParams enemyParams = new EnemyParams();
        enemyParams.canShoot = false;
        enemyParams.canThrow = true;
        enemyParams.autoAttackThrow = true;
        enemyParams.autoAttackShoot = false;
        enemyParams.stationaryAfterStop = true;
        enemyParams.intialAttackEnabled = false;
        enemyParams.maxHitPoints = 20;
        enemyParams.bulletProof = false;
        enemyParams.counterShoot = false;
        enemyParams.freezeZPos = false;
        SpawnEnemy(0f, enemyParams, roofTopSpawn);
        //roofTopEnemy = GameObject.Instantiate(_roofTopEnemy, roofTopSpawn.position, roofTopSpawn.rotation, GameObject.Find("Town").transform);
        //roofTopEnemy.GetComponent<EnemyController>().canShoot = false;
        //roofTopEnemy.GetComponent<EnemyController>().stationaryAfterStop = true;
        //roofTopEnemy.GetComponent<EnemyController>().attackEnabled = false;
        //roofTopEnemy.GetComponent<TargetController>().maxHitPoints = 20;
        //roofTopEnemy.GetComponent<EnemyController>().spawnPoint = roofTopSpawn;
        //roofTopEnemy.GetComponent<EnemyController>().bulletProof = false;
    }

    private void SpawnRoofTopEnemy2()
    {
        EnemyParams enemyParams = new EnemyParams();
        enemyParams.canShoot = true;
        enemyParams.canThrow = false;
        enemyParams.autoAttackShoot = true;
        enemyParams.autoAttackThrow = false;
        enemyParams.stationaryAfterStop = true;
        enemyParams.intialAttackEnabled = false;
        enemyParams.maxHitPoints = 20;
        enemyParams.bulletProof = false;
        enemyParams.counterShoot = false;
        enemyParams.freezeZPos = false;
        SpawnEnemy(0f, enemyParams, roofTopSpawn2);
        //roofTopEnemy2 = GameObject.Instantiate(_roofTopEnemy, roofTopSpawn2.position, roofTopSpawn2.rotation, GameObject.Find("Town").transform);
        //roofTopEnemy2.GetComponent<EnemyController>().canThrow = false;
        //roofTopEnemy2.GetComponent<EnemyController>().stationaryAfterStop = true;
        //roofTopEnemy2.GetComponent<EnemyController>().attackEnabled = false;
        //roofTopEnemy2.GetComponent<TargetController>().maxHitPoints = 20;
        //roofTopEnemy2.GetComponent<EnemyController>().spawnPoint = roofTopSpawn2;
        //roofTopEnemy2.GetComponent<EnemyController>().bulletProof = false;           
        
    }

    public void SpawnEnemy(float _delay, EnemyParams enemyParams, Transform spawnPoint)
    {
        IncrementEnemyCount();
        StartCoroutine(DoSpawn(_delay, enemyParams, spawnPoint));
    }

    IEnumerator DoSpawn(float delay, EnemyParams enemyParams, Transform spawnPoint)
    {
        yield return new WaitForSeconds(delay);
        GameObject enemy = GameObject.Instantiate(enemyPrefab, spawnPoint.position, spawnPoint.rotation, GameObject.Find("Town").transform);
        enemy.GetComponent<EnemyController>().canShoot = enemyParams.canShoot;
        enemy.GetComponent<EnemyController>().canThrow = enemyParams.canThrow;
        enemy.GetComponent<EnemyController>().autoAttackShoot = enemyParams.autoAttackShoot;
        enemy.GetComponent<EnemyController>().autoAttackThrow = enemyParams.autoAttackThrow;
        enemy.GetComponent<EnemyController>().stationaryAfterStop = enemyParams.stationaryAfterStop;
        enemy.GetComponent<EnemyController>().spawnPoint = spawnPoint;
        enemy.GetComponent<TargetController>().maxHitPoints = enemyParams.maxHitPoints;
        enemy.GetComponent<EnemyController>().counterShoot = enemyParams.counterShoot;
        enemy.GetComponent<EnemyController>().initialAttackEnabled = enemyParams.intialAttackEnabled;
        enemy.GetComponent<EnemyController>().bulletProof = enemyParams.bulletProof;
        enemy.GetComponent<EnemyController>().explosiveProof = enemyParams.explosiveProof;
        enemy.GetComponent<EnemyController>().freezeZPos = enemyParams.freezeZPos;
    }

    private bool GunsDrawn()
    {
        if (leftPistol.transform.forward.y < 0f &&
            leftPistol.transform.forward.z < 0.3f &&
            leftPistol.transform.forward.z > -0.3f &&
            rightPistol.transform.forward.y < 0f &&
            rightPistol.transform.forward.z < 0.3f &&
            rightPistol.transform.forward.z > -0.3f
            )
        {
            return false;
        }
        return true;
    }

    private void Update()
    {
        if (duelMode && inDuelingPosition && !readyForDuel && !duelStarted && waitForHolster)
        {
            if (!GunsDrawn())
            {
                holsterCount++;
                if (holsterCount == 180)
                {
                    StartDuel();
                }
            }
            else
            {
                holsterCount = 0;
            }
        }
        if (readyForDuel && !duelStarted && GunsDrawn())
        {
            DebugOnScreen("FOUL");
        }

        if (debugMode)
        {
            string txt;

            txt = leftPistol.transform.forward.ToString();
            txt += "\n";
            txt += rightPistol.transform.forward.ToString();
            txt += "\n";
            if (drawn)
            {
                txt += "DRAWN";
            }
            else
            {
                txt += "READY";
            }
            DebugOnScreen(txt);
        }
    }

    private void StartDuel()
    {
        readyForDuel = true;
        Debug.Log("STARTDUEL");
        DebugOnScreenType("Freeze!  Wait for the signal...Don't draw too early");
        Invoke("StartDuelFreeze", 5f);
        Invoke("SignalStartDuel", 10f);
    }

    private void StartDuelFreeze()
    {
        DebugOnScreen("");
    }

    private void SignalStartDuel()
    {
        PlaySound(duelStartedSoundClip);
        leftPistolController.GetComponent<shoot>().CanShoot = true;
        rightPistolController.GetComponent<shoot>().CanShoot = true;
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("enemy");
        foreach (GameObject e  in enemies) {
            if (e.GetComponent<EnemyController>())
            {
                e.GetComponent<EnemyController>().bulletProof = false;
            }
        }

        duelStarted = true;
        
    }

    public void EnableShowDownMode()
    {
        duelMode = true;
        waitForHolster = false;
        canvasController.ShowDebug();
        GameObject[] gos = GameObject.FindGameObjectsWithTag("enemy");
        CancelInvoke("SpawnHeart");
        CancelInvoke("SpawnLaser");
        foreach(GameObject g in gos)
        {
            if (g.transform.parent && g.transform.parent.tag != "enemy")
            {
                Destroy(g);
            }
        }
        DebugOnScreenType("It's time for a showdown!!!!\nGet Ready!");
        StartCoroutine(PsuedoPause(5f, "HOLSTER YOUR WEAPONS to get ready for the duel. (Just put them by your waist side pointing down)"));
        Invoke("EnableWaitForHolster", 15f);

        if (!easyMode)
        {
            EnableLaserSights(false);
        }
        SpawnDueler();       
        duelStarted = false;
        holsterCount = 0;
        readyForDuel = false;
        leftPistolController.GetComponent<shoot>().CanShoot = false;
        rightPistolController.GetComponent<shoot>().CanShoot = false;
        leftPistolController.GetComponent<shoot>().AmmoCount = 1;
        rightPistolController.GetComponent<shoot>().AmmoCount = 1;
        leftPistolController.GetComponent<shoot>().maxAmmo = 1;
        rightPistolController.GetComponent<shoot>().maxAmmo = 1;
        leftPistolController.GetComponent<shoot>().AutoReload = false;
        rightPistolController.GetComponent<shoot>().AutoReload = false;
    }

    private void EnableWaitForHolster()
    {
        waitForHolster = true;
    }

    private GameObject SpawnDueler()
    {
        GameObject enemy = GameObject.Instantiate(enemyPrefab, groundEnemySpawnPoint.transform.position, groundEnemySpawnPoint.transform.rotation, GameObject.Find("Town").transform);
        EnemyController ec = enemy.GetComponent<EnemyController>();
        ec.autoAttackShoot = false;
        ec.autoRespawn = false;
        ec.autoAttackThrow = false;
        ec.canThrow = false;
        ec.canShoot = true;
        ec.stationaryAfterStop = false;
        ec.initialAttackEnabled = false;
        ec.freezeZPos = false;
        ec.explosiveProof = false;
        ec.bulletProof = true;
        ec.spawnPoint = groundEnemySpawnPoint;
        enemy.GetComponent<TargetController>().maxHitPoints = 1;
        ec.Dueling = true;
        return enemy;
    }

    IEnumerator PsuedoPause(float amtTime, string txt)
    {
        yield return new WaitForSeconds(amtTime);
        DebugOnScreenType(txt);
    }

    private void DebugOnScreenType(string txt) 
    {
        StartCoroutine(TypeToScreen(txt));
    }

    IEnumerator TypeToScreen(string txt)
    {
        int i;
        i = 0;
        while (i < txt.Length)
        {
            DebugOnScreen(txt.Substring(0, i));
            i++;
            yield return new WaitForSeconds(0.05f);
        }
    }

    public void PlaySound(AudioClip soundClip)
    {
        soundController.PlayClip(soundClip);
    }

    private void FixedUpdate()
    {
        if (timeSlowed)
        {
            timeBank--;
            if (timeBank <= 0)
            {
                timeBank = 0;
                ChangeTimeScale(1f);
            }
            canvasController.SetTimeScaleSlider(timeBank);
        }
    }

    private void SpawnHeart()
    {
        Vector3 where = new Vector3();
        where.x = Random.Range(heartSpawnLocation.transform.position.x - heartSpawnVolumeRadii.x, heartSpawnLocation.transform.position.x + heartSpawnVolumeRadii.x);
        where.y = Random.Range(heartSpawnLocation.transform.position.y - heartSpawnVolumeRadii.y, heartSpawnLocation.transform.position.y + heartSpawnVolumeRadii.y);
        where.z = Random.Range(heartSpawnLocation.transform.position.z - heartSpawnVolumeRadii.z, heartSpawnLocation.transform.position.z + heartSpawnVolumeRadii.z);
        Destroy(GameObject.Instantiate(heart, where, Quaternion.identity), 30f);
    }

    private void SpawnLaser()
    {
        Vector3 where = new Vector3();
        where.x = Random.Range(heartSpawnLocation.transform.position.x - heartSpawnVolumeRadii.x, heartSpawnLocation.transform.position.x + heartSpawnVolumeRadii.x);
        where.y = Random.Range(heartSpawnLocation.transform.position.y - heartSpawnVolumeRadii.y, heartSpawnLocation.transform.position.y + heartSpawnVolumeRadii.y);
        where.z = Random.Range(heartSpawnLocation.transform.position.z - heartSpawnVolumeRadii.z, heartSpawnLocation.transform.position.z + heartSpawnVolumeRadii.z);
        Destroy(GameObject.Instantiate(laserPowerUp, where, Quaternion.identity), 30f);
    }

    public void GetLaser()
    {
        EnableLaserSights(true);
    }

    public void AddOrSubtractHitPoints(int amt)
    {
        playerHitPoints -= amt;
        if (playerHitPoints > maxHP)
        {
            playerHitPoints = maxHP;
        } 
        if (playerHitPoints <= 0)
        {
            playerHitPoints = 0;
            DoDeath();
        }
        canvasController.AdjustHitPoints(playerHitPoints);
    }

    private void DoDeath()
    {

    }

    public void GetSlow()
    {
        if (timeBank > 0 && !timeBankRefilling)
        {
            CancelInvoke("RefillTimeBank");
            ChangeTimeScale(.5f);
        }
    }

    public void ResumeTime()
    {
        ChangeTimeScale(1f);
        Invoke("RefillTimeBank", 5f);
    }

    private void RefillTimeBank()
    {
        if (timeBank < maxTimeBank)
        {
            StartCoroutine(DoRefillTimeBank());
        }
    }

    IEnumerator DoRefillTimeBank()
    {
        while (timeBank < maxTimeBank)
        {
            timeBankRefilling = true;
            timeBank += 2;
            canvasController.SetTimeScaleSlider(timeBank);
            canvasController.SetTimeScaleSliderColor(Color.red);
            yield return null;
        }
        timeBankRefilling = false;
        canvasController.SetTimeScaleSliderColor(Color.white);
    }

    public void DisableAllPowerUps()
    {
        ChangeTimeScale(1f);
        EnableLaserSights(false);
    }

    public void addHit(int hitAmt)
    {
        dynamiteHits++;
    }

    public void addScore(int amt)
    {
        if (isInGame)
        {
            canvasController.AddScore(amt);
        }
    }

    public void ChangeTimeScale(float targetScale)
    {
        StartCoroutine(_ChangeTimeScale(targetScale));
    }

    IEnumerator _ChangeTimeScale(float targetScale)
    {
        float scaleIncrements = (targetScale - Time.timeScale) / 60f;
        if (scaleIncrements < 0f)
        {
            while ((Time.timeScale - targetScale) > 0f)
            {
                Time.timeScale = Time.timeScale + scaleIncrements;
                musicController.GetComponent<AudioSource>().pitch = Time.timeScale;
                yield return null;
            }
            Time.timeScale = targetScale;
            musicController.GetComponent<AudioSource>().pitch = Time.timeScale;
            timeSlowed = true;
        }
        else
        {
            timeSlowed = false;
            while ((Time.timeScale - targetScale) < 0f)
            {
                Time.timeScale = Time.timeScale + scaleIncrements;
                musicController.GetComponent<AudioSource>().pitch = Time.timeScale;
                yield return null;
            }
            Time.timeScale = targetScale;
            musicController.GetComponent<AudioSource>().pitch = Time.timeScale;
        }

    }

    public void SetLaserSightsBattery(int amt)
    {
        StartCoroutine(DrainLaserBattery(amt));
    }

    IEnumerator DrainLaserBattery(int amt)
    {
        if (laserBatteryAmt == 0)
        {
            laserBatteryAmt = amt;
            if (laserBatteryAmt > laserBatteryMax)
            {
                laserBatteryAmt = laserBatteryMax;
            }
            EnableLaserSights(true);
            while (laserBatteryAmt > 0)
            {
                laserBatteryAmt--;
                canvasController.SetLaserSlider(laserBatteryAmt);
                yield return null;
            }
            EnableLaserSights(false);
        } else
        {
            laserBatteryAmt += amt;
            if (laserBatteryAmt > laserBatteryMax)
            {
                laserBatteryAmt = laserBatteryMax;
            }
        }
    }

    public void EnableLaserSights(bool on)
    {
        leftLasterSight.SetActive(on);
        rightLaserSight.SetActive(on);
    }

    public void SetHits(int amt)
    {
        dynamiteHits = amt;
        if (dynamiteHits == 0)
        {
            ChangeTimeScale(1f);
            leftLasterSight.SetActive(false);
            rightLaserSight.SetActive(false);
        }        
    }

    public void ShowFloatingScore(Vector3 where, int points)
    {
        if (points > 0 && isInGame)
        {
            GameObject _floatingScore = GameObject.Instantiate(floatingScore, where, Quaternion.identity);
            _floatingScore.transform.LookAt(player.transform.position, Vector3.up);
            _floatingScore.transform.Rotate(Vector3.up, 180f);

            _floatingScore.GetComponent<FloatingScoreController>().SetText("+" + points.ToString());
        }
    }   
}
