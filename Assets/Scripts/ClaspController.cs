﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClaspController : MonoBehaviour {

    public GameObject[] barrels;
    public GameObject barrelExplosiveEffect;
    public AudioClip ExplosionSound;

	// Use this for initialization
	void Start () {
        GetComponent<TargetController>().onHit += HandleDestruction;	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void HandleDestruction(Vector3 where, Vector3 normal, Transform shooter, int damage, float forceAmt)
    {
        for (int i = 0; i < barrels.Length; i++)
        {
            barrels[i].GetComponent<TargetController>().hitEffect = barrelExplosiveEffect;
            barrels[i].GetComponent<TargetController>().hitSound[0] = ExplosionSound;
            barrels[i].GetComponent<TargetController>().points = 50;
            barrels[i].GetComponent<BarrelController>().isPrimed = true;
        }        
    }
}