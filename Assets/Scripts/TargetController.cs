﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetController : MonoBehaviour {

    public bool isLaser = false;
    public bool isPowerOff = false;
    public bool isSlow = false;
    public bool isPlayer = false;
    public AudioClip[] hitSound;
    private List<AudioSource> spawnedSound = new List<AudioSource>();
    public GameObject hitEffect = null;
    private SceneController sceneController;
    public int hitPoints = 0;
    public int maxHitPoints = 0;
    public int points = 0;
    private GameObject player;
    public bool hasCollisionPhysics = false;
    public float forceScale = 1f;
    public Vector3 forceAdditionMin;
    public Vector3 forceAdditionMax;
    public int damagePerHit;
    public bool damageOverride = false;
    private GameObject audioSource;
    public bool dead = false;
    public bool isEasyModeStart = false;
    public bool disappearAfterHit = false;
    public bool isNormalModeStart = false;

    public delegate void OnHit(Vector3 contact, Vector3 normal, Transform shooterPosition, int damage, float force);
    public event OnHit onHit;

    private void OnDestroy()
    {
        if (audioSource)
        {
            Destroy(audioSource, 5f);
        }
    }
    void Awake() {
        sceneController = GameObject.Find("SceneController").GetComponent<SceneController>();
        if (isPowerOff || isLaser || isSlow)
        {
            StartCoroutine(Spin(1f));
        }
        player = sceneController.GetComponent<SceneController>().GetPlayer();
        onHit = null;
        transform.gameObject.AddComponent<AudioSource>();
        if (hitSound.Length > 0)
        {
            audioSource = new GameObject(transform.gameObject.name + " local audiosource");
            audioSource.AddComponent<AudioSource>();
            audioSource.GetComponent<AudioSource>().playOnAwake = false;
            audioSource.GetComponent<AudioSource>().loop = false;
        }
        hitPoints = maxHitPoints;
	}

    IEnumerator Spin(float spinSpeed)
    {
        Vector3 rotation = new Vector3(0f, 0f, 0f);
        while (true)
        {
            rotation.Set(0f, 0f, spinSpeed);
            transform.Rotate(rotation);
            yield return null;
        }
    }

    public void DoHit(Vector3 where, Vector3 normal, Transform shooterPosition, int incomingDamage, float forceAmt = 0f)
    {
        if (!dead)
        {
            int damageAmt = 0;
            if (damageOverride)
            {
                damageAmt = damagePerHit;
            }
            else
            {
                damageAmt = incomingDamage;
            }
            hitPoints -= damageAmt;
            sceneController.ShowFloatingScore(where, points);
            sceneController.addScore(points);
            if (hitSound.Length > 0)
            {
                int idx = Random.Range(0, hitSound.Length);
                if (hitSound[idx] != null)
                {
                    audioSource.GetComponent<AudioSource>().clip = hitSound[idx];
                    audioSource.GetComponent<AudioSource>().Play();
                }
            }
            if (hitEffect != null)
            {
                GameObject.Instantiate(hitEffect, where, Quaternion.Euler(-1f * shooterPosition.transform.forward));
            }
            if (isLaser)
            {
                sceneController.GetLaser();
            }
            if (isSlow)
            {
                sceneController.GetSlow();
            }
            if (isPowerOff)
            {
                sceneController.DisableAllPowerUps();
            }
            if (onHit != null)
            {
                onHit(where, normal, shooterPosition, incomingDamage, forceAmt);
            }
            if (hasCollisionPhysics)
            {
                if (transform.GetComponent<Rigidbody>() != null)
                {
                    Vector3 forceAddition = new Vector3();
                    float x, y, z;
                    x = Random.Range(forceAdditionMin.x, forceAdditionMax.x);
                    y = Random.Range(forceAdditionMin.y, forceAdditionMax.y);
                    z = Random.Range(forceAdditionMin.z, forceAdditionMax.z);
                    forceAddition.Set(x, y, z);
                    //transform.GetComponent<Rigidbody>().AddForce(shooterPosition.transform.forward * forceAmt * forceScale);
                    transform.GetComponent<Rigidbody>().AddForceAtPosition((shooterPosition.transform.forward * forceAmt * forceScale) + forceAddition, where);
                }
            }
            if (hitPoints < 0 && !isPlayer && !dead && sceneController.IsInGame)
            {
                Destroy(transform.gameObject);
            }
            if (isPlayer)
            {
                sceneController.AddOrSubtractHitPoints(damageAmt);
            }
            if (isEasyModeStart)
            {
                sceneController.StartEasyMode();
                if (disappearAfterHit)
                {
                    transform.gameObject.SetActive(false);
                    if (gameObject.tag == "bullseye")
                    {
                        transform.parent.gameObject.SetActive(false);
                    }
                }
            }
            if (isNormalModeStart)
            {
                sceneController.StartNormalMode();
                if (disappearAfterHit)
                {
                    transform.gameObject.SetActive(true);
                }
            }
        }
    }
}
