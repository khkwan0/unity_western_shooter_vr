﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FloatingScoreController : MonoBehaviour {

    public Vector3 direction;
    private GameObject textObject;
    private TextMeshPro textArea;
    public float duration = 1;

	void Awake() {
        textObject = transform.Find("TextMeshPro").gameObject;
        textArea = textObject.GetComponent<TextMeshPro>();
        StartCoroutine(DoMove());
	}

    public void SetText(string text)
    {
        textArea.text = text;
    }

    IEnumerator DoMove()
    {
        float startTime = Time.time;
        float inc;        
        while (Time.time - startTime < duration)
        {
            inc = (Time.time - startTime) / duration;
            textArea.color = new Color(textArea.color.r, textArea.color.g, textArea.color.b, 1f - inc);
            transform.position = transform.position + direction;
            yield return null;
        }
        Destroy(transform.gameObject);
    }
}
