﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnAroundTriggerController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "enemy")
        {
            other.transform.parent.transform.Rotate(new Vector3(0f, 180f, 0f));
        }
    }
}
