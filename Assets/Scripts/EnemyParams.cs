﻿using System.Collections;
using System.Collections.Generic;

public class EnemyParams
{
    public bool canShoot;
    public bool canThrow;
    public bool autoAttackShoot;
    public bool autoAttackThrow;
    public bool stationaryAfterStop;
    public bool attackEnabled;
    public bool intialAttackEnabled;
    public bool counterShoot;
    public bool autoRespawn;
    public bool bulletProof;
    public bool explosiveProof;
    public int maxHitPoints;
    public bool freezeZPos;
}
