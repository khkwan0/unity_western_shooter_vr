﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoofTopTurnPointController : MonoBehaviour {

    public Transform nextPoint;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "enemy")
        {
            other.gameObject.transform.parent.transform.LookAt(nextPoint);
        }
    }
}
