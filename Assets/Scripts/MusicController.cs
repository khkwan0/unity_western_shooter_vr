﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicController : MonoBehaviour {

    public AudioClip[] tracks;

    public void PlayIntro()
    {
        GetComponent<AudioSource>().clip = tracks[0];
        GetComponent<AudioSource>().Play();
    }

    public void PlayNormal()
    {
        GetComponent<AudioSource>().clip = tracks[1];
        GetComponent<AudioSource>().Play();
    }
}
