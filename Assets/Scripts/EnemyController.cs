﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    public GameObject dynamitePrefab;
    public GameObject player;
    public GameObject projectileSpawnPoint;
    public GameObject bullet;
    public GameObject bulletSpawnPoint;
    public GameObject shotEffect;
    public float bulletSpeed = 4f;
    public AudioClip gunshotSound;
    public float bulletChance = 0.5f;
    public float dynamiteChance = 0.5f;
    private SceneController sceneController;
    private Animator animator;
    private Vector3 originalPos;
    private Quaternion originalRot;
    public bool canShoot = true;
    public bool canThrow = true;
    public bool autoAttackShoot = true;
    public bool autoAttackThrow = true;
    public bool bulletProof = true;
    public bool explosiveProof = false;
    public bool stationaryAfterStop = false;
    public bool initialAttackEnabled = false;
    [SerializeField]
    private bool attackEnabled = false;
    public GameObject deathEffect;
    public bool counterShoot = false;
    private bool isAttacking = false;
    public Transform spawnPoint;
    [SerializeField]
    private bool isDead;
    public bool autoRespawn = true;
    public float repeatRateThrow = 5f;
    public float repeatRateShoot = 5f;
    public float easyModeScale = 0.5f;
    public bool freezeZPos = false;
    [SerializeField]
    private bool dueling;

    public bool Dueling
    {
        set { dueling = value; }
        get { return dueling; }
    }
    
    public bool IsDead
    {
        set { isDead = value; }
        get { return isDead; }
    }

    public bool AttackEnabled
    {
        set { attackEnabled = value; }
        get { return attackEnabled; }
    }

    // Use this for initialization
    void Start () {
        sceneController = GameObject.Find("SceneController").GetComponent<SceneController>();

        if (autoAttackThrow && canThrow && attackEnabled && !stationaryAfterStop)
        {
            isAttacking = true;
            if (!sceneController.EasyMode)
            {
                easyModeScale = 1f;
            }
            InvokeRepeating("MovingLaunchProjectile", 5f, repeatRateThrow/easyModeScale);
        }
        if (autoAttackShoot  && canShoot && attackEnabled && !stationaryAfterStop)
        {
            isAttacking = true;
            if (!sceneController.EasyMode)
            {
                easyModeScale = 1f;
            }
            InvokeRepeating("WalkingShootAtPlayer", 2f, repeatRateShoot/easyModeScale);
        }
        if (GetComponent<TargetController>())
        {                    
            GetComponent<TargetController>().onHit += DoHit;        
        }
        animator = GetComponent<Animator>();
        player = GameObject.Find("Camera (eye)");
        isDead = false;
        attackEnabled = initialAttackEnabled;
        if (freezeZPos)
        {
            GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ;
        }
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
    }

    private void Update()
    {
        if (dueling)
        {
            if (transform.position.x == player.transform.position.x)
            {
                animator.StopPlayback();
                transform.LookAt(player.transform);
            }

        }
    }
    public void StopMoving()
    {
        if (!isDead && !isAttacking)
        {
            animator.Play("Idle_Neutral_1");
            if (canShoot && attackEnabled)
            {
                isAttacking = true;
                ShootNow();
                if (autoAttackShoot)
                {
                    InvokeRepeating("MaybeShoot", 5f, 5f);
                }
            }
            if (canThrow && attackEnabled)
            {
                isAttacking = true;
                LaunchProjectile();
                if (autoAttackThrow)
                {
                    InvokeRepeating("MaybeLaunch", 5f, 5f);
                }
            }
        }
    }

    public void TakeExplosiveDamage(int damageAmt)
    {
        if (!explosiveProof)
        {
            GetComponent<TargetController>().hitPoints -= damageAmt;
            if (GetComponent<TargetController>().hideFlags < 0)
            {
                DoDie();
            }
        }
    }

    public void DoHit(Vector3 where, Vector3 normal, Transform shooterPosition, int damage, float force)
    {
        if (canShoot && attackEnabled && counterShoot && !sceneController.EasyMode)
        {
            if (animator.GetCurrentAnimatorStateInfo(0).IsName("Walking"))
            {
                ShootNow();
            }
        }
        if (!bulletProof)
        {
            GetComponent<TargetController>().hitPoints -= damage;
            if (GetComponent<TargetController>().hitPoints < 0)
            {
                DoDie();
            }
        }
    }

    public void DoDie()
    {
        CancelInvoke();
        isDead = true;
        GetComponent<TargetController>().dead = isDead;
        animator.Play("FallBack");
        GameObject.Instantiate(deathEffect, transform.position, Quaternion.identity);
        if (autoRespawn)
        {
            AutoRespawn();
        }
        Destroy(transform.gameObject, 5f);
    }

    private void AutoRespawn()
    {
        EnemyParams enemyParams = new EnemyParams();
        enemyParams.autoAttackShoot = autoAttackShoot;
        enemyParams.autoAttackThrow = autoAttackThrow;
        enemyParams.stationaryAfterStop = stationaryAfterStop;
        enemyParams.canShoot = canShoot;
        enemyParams.canThrow = canThrow;
        enemyParams.maxHitPoints = GetComponent<TargetController>().maxHitPoints;
        enemyParams.counterShoot = counterShoot;
        enemyParams.autoRespawn = autoRespawn;
        enemyParams.bulletProof = bulletProof;
        enemyParams.explosiveProof = explosiveProof;
        enemyParams.intialAttackEnabled = initialAttackEnabled;
        sceneController.SpawnEnemy(Random.Range(10f, 20f), enemyParams, spawnPoint);
    }

    private void ShootNow()
    {
        originalPos.Set(transform.position.x, transform.position.y, transform.position.z);
        originalRot.Set(transform.rotation.x, transform.rotation.y, transform.rotation.z, transform.rotation.w);
        ShootAtPlayer(Vector3.zero, Vector3.zero, null, 0, 0f);
    }

    private void MaybeShoot()
    {       
        if (Random.Range(0f, 1f) < (bulletChance * easyModeScale))
        {
            ShootNow();
        }
    }

    private void WalkingShootAtPlayer()
    {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Walking"))
        {
            MaybeShoot();
        }
    }

    private void ShootAtPlayer(Vector3 where, Vector3 normal, Transform shooterPosition, int damage, float force)
    {
        //GetComponent<Rigidbody>().freezeRotation = true;        
        animator.Play("Shooting", 0, 0.1f);
        transform.LookAt(player.transform);
        //Debug.Log(originalRot.eulerAngles);
        StartCoroutine(DoShoot());
    }

    IEnumerator DoShoot()
    {
        yield return new WaitForSeconds(1.3f);
        GameObject _bullet = GameObject.Instantiate(bullet, bulletSpawnPoint.transform.position, Quaternion.identity);
        GameObject _sf;
        _bullet.GetComponent<BulletController>().Damage = 20;
        _bullet.GetComponent<BulletController>().Target = player;
        _bullet.GetComponent<BulletController>().Shoot(bulletSpeed);
        _sf = GameObject.Instantiate(shotEffect, bulletSpawnPoint.transform.position, Quaternion.identity);
        _sf.GetComponent<ParticleSystem>().Play();
        sceneController.PlaySound(gunshotSound);
        yield return null;
        ResetAnimation();
    }

    IEnumerator BackToNormal(string animTag)
    {
        while (animator.GetCurrentAnimatorStateInfo(0).IsName("Shooting") || animator.GetCurrentAnimatorStateInfo(0).IsName("Throwing"))
        {
            yield return null;
        }
        animator.Play(animTag);
        /*
        transform.position = originalPos;
        */
        transform.rotation = originalRot;
        //GetComponent<Rigidbody>().freezeRotation = false;
        
    }

    private void MaybeLaunch()
    {
        if (Random.Range(0f, 1f) < (dynamiteChance * easyModeScale))
        {
            LaunchProjectile();
        }
    }

    private void MovingLaunchProjectile()
    {
        if (Random.Range(0f, 1f) < dynamiteChance && animator.GetCurrentAnimatorStateInfo(0).IsName("Walking"))
        {
            LaunchProjectile();
        }
    }

    private void LaunchProjectile()
    {
        //originalPos.Set(transform.position.x, transform.position.y, transform.position.z);
        originalRot.Set(transform.rotation.x, transform.rotation.y, transform.rotation.z, transform.rotation.w);
        transform.LookAt(player.transform);
        animator.Play("Throwing", 0, 0.1f);
        StartCoroutine(DoThrow());
    }

    IEnumerator DoThrow()
    {
        yield return new WaitForSeconds(.8f);
        float randomAngle = Random.Range(30f, 60f);
        float targetDistance;
        float i, j, v;
        GameObject dynamite = GameObject.Instantiate(dynamitePrefab, projectileSpawnPoint.transform.position, Quaternion.identity);
        //targetDistance = Vector3.Distance(dynamite.transform.position, player.transform.position);
        targetDistance = Mathf.Sqrt(Mathf.Pow(dynamite.transform.position.x - player.transform.position.x, 2) + Mathf.Pow(dynamite.transform.position.z - player.transform.position.z, 2));
        dynamite.transform.LookAt(player.transform);
        dynamite.transform.rotation = Quaternion.Euler(-1f * randomAngle, dynamite.transform.rotation.eulerAngles.y, 0f);
        //Debug.Log("Random Angle:" + randomAngle);
        randomAngle = Mathf.Deg2Rad * randomAngle;
        i = (dynamite.transform.position.y - player.transform.position.y) + targetDistance / Mathf.Cos(randomAngle);
        //Debug.Log(targetDistance);
        //Debug.Log(Mathf.Cos(randomAngle));
        j = Mathf.Pow(targetDistance / Mathf.Cos(randomAngle), 2) * -4.9f;
        i = Mathf.Sin(randomAngle) * i * -1f;
        v = Mathf.Sqrt(j / i);
        //Debug.Log("ANgle: " + randomAngle);
        //Debug.Log(i / j);
        //Debug.Log(v);

        //dynamite.transform.RotateAround(dynamite.transform.position, transform.right, randomAngle);
        //Time.timeScale = 0f;
        dynamite.GetComponent<Rigidbody>().velocity = dynamite.transform.forward * v;
        //dynamite.GetComponent<Rigidbody>().AddForce(dynamite.transform.forward * 500f * 1 / Time.timeScale + dynamite.transform.up * 750f * 1 / Time.timeScale);
        dynamite.GetComponent<Rigidbody>().AddTorque(new Vector3(Random.value * 50f, 0f, Random.value * 50f));
        yield return null;
        ResetAnimation();
    }

    private void ResetAnimation()
    {
        if (!isDead)
        {
            if (stationaryAfterStop)
            {
                StartCoroutine(BackToNormal("Idle_Neutral_1"));
            }
            else
            {
                StartCoroutine(BackToNormal("Walking"));
            }
        }
    }
}
