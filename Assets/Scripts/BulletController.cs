﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {

    public float timeToLive;

    [SerializeField]
    private int damage = 0;
    [SerializeField]
    private GameObject target;
    private float bulletSpeed = 0f;

    private bool isNerfed = false;
    public AudioSource bulletFlyTone;

    public int Damage
    {
        get { return damage; }
        set { damage = value; }
    }

    public GameObject Target { 
        set { target = value; transform.LookAt(target.transform.position); }
        get { return target; }
    }

    private void Start()
    {
        if (GetComponent<TargetController>() != null)
        {
            GetComponent<TargetController>().onHit += NerfBullet;
        }
        GameObject.Instantiate(bulletFlyTone, transform);
    }

    public void NerfBullet(Vector3 where, Vector3 normal, Transform shooterPosition, int incomingDamage, float forceAmt)
    {
        GetComponent<Rigidbody>().isKinematic = false;
        GetComponent<Rigidbody>().useGravity = true;
        bulletFlyTone.Stop();
        bulletSpeed = 0f;
        isNerfed = true;
    }

    public void Shoot(float speed)
    {
        bulletSpeed = speed;
        StartCoroutine(BulletTravel());
    }

    public void OnCollisionEnter(Collision collision)
    {
        TargetController _target = collision.collider.GetComponent<TargetController>();
        if (_target == null && collision.collider != null && collision.collider.transform != null && collision.collider.transform.parent != null)
        {
            _target = collision.collider.transform.parent.GetComponent<TargetController>();
        }
        if (_target != null)
        {
            _target.DoHit(collision.contacts[0].point, collision.contacts[0].normal, transform, damage);
        }
        if (collision.collider.tag == "head" || collision.collider.tag == "body")
        {
            Destroy(transform.gameObject);
        }
    }

    IEnumerator BulletTravel()
    {
        float startTime = Time.time;
        while (Time.time - startTime < timeToLive)
        {
            if (!isNerfed)
            {
                GetComponent<Rigidbody>().velocity = transform.forward * bulletSpeed;
            }
            yield return null;
        }
        Destroy(transform.gameObject);
    }
}
