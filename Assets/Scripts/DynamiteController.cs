﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamiteController : MonoBehaviour {

    public GameObject explosion;
    public AudioClip[] explosionSounds;
    public float ttl = 5f;
    public int damage;

	// Use this for initialization
	void Start () {
        Invoke("SelfExplode", ttl);
	}

    public void SelfExplode()
    {
        Explode();
        Destroy(transform.gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "explosion_area")
        {
            collision.collider.transform.gameObject.GetComponent<TargetController>().DoHit(collision.contacts[0].point, collision.contacts[0].normal, transform, damage);
            //GameObject.Instantiate(explosion, transform.position, Quaternion.identity);
            Destroy(transform.gameObject);
        }
    }

    public void Explode()
    {
        GameObject.Instantiate(explosion, transform.position, Quaternion.identity);   
        GameObject.Find("SceneController").GetComponent<SceneController>().PlaySound(explosionSounds[Random.Range(0, explosionSounds.Length)]);
    }
}
